let divCount = 0;

document.querySelector('#btn').onclick = (e) => {
  e.preventDefault();
  createDiv();
}

const createDiv = () => {
  let div = document.createElement('div');
  div.textContent = "Lorem ipsum dolor sit amet consectetur adipisicing elit.";
  div.classList.add('div')
  if(divCount != 10) {
    document.body.appendChild(div);
    divCount++;
  } else {
    let divs = document.querySelectorAll('.div');
    divs.forEach(div => {
      div.remove();
    })
    divCount = 0;
  }
}