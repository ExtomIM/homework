const content = document.querySelector('.all-currency')

const getResource = async (url) => {
  let res = await fetch(url);

  if(!res.ok) {
    content.innerHTML = `${new Error(`Could not fetch ${url}, status ${res.status}`)}`;
  }

  return await res.json();
}

const getCurrency = async () => {
  const res = await getResource("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json")
  return res.map(transformCurrency);
}
console.log(getCurrency());

const showCurrency = () => {
  getCurrency()
  .then(element => {
    for(let i = 0; i < element.length; i++) {
      if(element[i].rate < 25) continue;
      content.innerHTML += `<div class="currency-element"><div class="currency-name">${element[i].txt}</div> - <div class="currency-rate">${element[i].rate}</div></div>`
    }
  })
  
}

showCurrency()


const transformCurrency = (curr) => {
  return {
    txt: curr.txt,
    rate: curr.rate
  }
}
