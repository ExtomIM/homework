import React from "react";

import './footer.css'

const Footer = () => {
  return (
    <div className="footer">
      <p className="footer-text">
        Copyright by phototime - all right reserved
      </p>
    </div>
  )
}

export default Footer;