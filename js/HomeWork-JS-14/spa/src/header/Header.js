import React from "react";

import './Header.css'

const Header = () => {
  return (
    <div className="header">
      <div className="header-nav">
        <a href="" className="header-item">home</a>
        <a href="" className="header-item">photoapp</a>
        <a href="" className="header-item">design</a>
        <a href="" className="header-item">download</a>
      </div>
    </div>
  )
}

export default Header;