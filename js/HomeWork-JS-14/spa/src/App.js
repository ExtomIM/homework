import React from "react";

import Header from './header/Header';
import MainImg from "./Main-img/Main-img";
import TextBlock from "./text-block/text-block";
import Footer from "./footer/footer";

const App = () => {
  return (
    <>
      <Header/>
      <MainImg/>
      <TextBlock/>
      <Footer/>
    </>
  )
}

export default App;