import React from "react";

import './text.css'

const TextBlock = () => {
  return (
    <div className="text-block">
      <div className="text-block__wrapper">
        <h2 className="text-block__title">rappresent your life with a simple photo</h2>
        <p className="text-block__p">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex dignissimos pariatur laboriosam autem. Natus velit sapiente sed fugiat! Ab mollitia, placeat sint quasi blanditiis ratione facere laborum assumenda nobis neque?
          Vitae itaque libero cupiditate beatae ad, magnam eveniet quae voluptatem doloremque corporis nisi, voluptatum porro dolorem est aliquam illum, quis eaque accusantium quia repudiandae nostrum! Earum unde impedit repudiandae quasi.
          Blanditiis quia doloribus quis pariatur earum numquam possimus eum maiores similique expedita ratione, facere sed inventore suscipit, sint eveniet iure nam dicta, adipisci id dolorum fuga natus! Earum, a repudiandae.
        </p>
        <p className="text-block__p">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur modi ea nesciunt suscipit, facilis sunt recusandae? Nam voluptatibus, aliquam laborum nemo dignissimos cumque modi eaque mollitia quia, nesciunt officia? Soluta.
          Exercitationem cupiditate esse nihil amet, consectetur quisquam architecto corporis quae assumenda porro, quaerat possimus laboriosam dolore aut eaque ratione dolorem iste ut atque illum perferendis eveniet, quos totam ab. Repellat?
        </p>
      </div>
      <button className="text-block__btn">get started</button>
    </div>
  )
}

export default TextBlock;