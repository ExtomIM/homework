const save = document.querySelector('.save'),
      select = document.querySelector('.select'),
      saveAll = document.querySelector('.select-all');

window.addEventListener('keydown', (e) => {
  e.preventDefault();
  console.log(e)
  if(e.ctrlKey === true && e.key === 's') {
    save.classList.add('visible');
    setTimeout(() => {
      save.classList.remove('visible');
    }, 2000)
  }
  if(e.ctrlKey === true && e.key === 'a') {
    select.classList.add('visible');
    setTimeout(() => {
      select.classList.remove('visible');
    }, 2000)
  }
  if(e.ctrlKey === true && e.shiftKey === true && e.key === 'S') {
    saveAll.classList.add('visible');
    setTimeout(() => {
      saveAll.classList.remove('visible');
    }, 2000)
  }
})