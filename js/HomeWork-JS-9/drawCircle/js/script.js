const inputDiametr = document.querySelector('.add-content'),
      startButtton = document.querySelector('.start-button'),
      inputValue = document.querySelector('.circle-diametr'),
      drawCircle = document.querySelector('.draw-circle'),
      circleWrapper = document.querySelector('.circle-wrapper');
let isCircles = false;

startButtton.addEventListener('click', () => {
  inputDiametr.classList.add('d-block')
})

drawCircle.addEventListener('click', () => {
  createCircle();
  deleteCircle();
})

function createCircle() {
  for(let i = 0; i < 100; i++) {
    const circle = document.createElement('div');
    circle.classList.add('circle');
    circle.style.width = inputValue.value + 'px';
    circle.style.height = inputValue.value + 'px';
    circleWrapper.appendChild(circle);
  }
}

function deleteCircle() {
  document.querySelectorAll('.circle').forEach(circle => {
    circle.addEventListener('click', () => {
      circle.classList.add('d-none');
    })
  })
}




