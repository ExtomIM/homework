const SIZE_SMALL = {name: 'small', kkal: 20, price:50},
      SIZE_LARGE = {name: 'large', kkal: 40, price:100},
      STUFFING_CHEESE = {name: 'cheese', kkal: 20, price:10},
      STUFFING_SALAD = {name:'salad', kkal:5, price:20},
      STUFFING_POTATO = {name:'potato', kkal:10, price:15},
      TOPPING_MAYO = {name:'mayo', kkal:5, price:20},
      TOPPING_SPICE = {name: 'spice', kkal:0, price:15}


class Hamburger {
  constructor(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.allTopping = [];
  }

  addTopping(topping) {

    for (let top of Object.values(this.allTopping)){
      if (top.name === topping.name) {
        console.log('This topping added alredy')
        return
      }
    };
    if(topping === TOPPING_MAYO || TOPPING_SPICE) {
      this.allTopping.push(topping);
    } else {
      new Error('This topping not avialiable')
    }
  }

  getSize() {
    return this.size;
  }

  setSize(size) {
    if(size === SIZE_LARGE || size === SIZE_SMALL) {
      this.size = size;
    } else {
      console.log(
        new Error('Faliure. Wrong data'))
    }
  }

  removeTopping(topping) {
    if(topping === TOPPING_MAYO || topping === TOPPING_SPICE){
      this.allTopping = this.allTopping.filter(top => top.name != topping.name)
    } else {
      console.log(
        new Error('This topping not avialble')
      )
    }
  }

  getTopping() {
    return this.allTopping;
  }

  getStuffing() {
    return this.stuffing;
  }

  setStuffin(stuffing) {
    if (stuffing === STUFFING_CHEESE || stuffing === STUFFING_SALAD || stuffing === STUFFING_POTATO) {
      this.stuffing = stuffing;
    } else {
      new Error('Stuffing not avialble')
    }

    try {
      if (!stuffing) {
        throw new Error('Faliure data')
      }
    }
    catch(error) {
      console.log(error)
    }
  }

  calculatePrice() {
    let sumPrice = 0;

    for(let item of Object.values(this)) {
      if(item.price) {
        sumPrice += +item.price;
      }
    }
    return sumPrice;
  }

  calculateCalories() {
    let sumKkal = 0;

    for(let item of Object.values(this)) {
      if(item.kkal) {
        sumKkal += item.kkal;
      }
    }
    for(let item of Object.values(this.allTopping)) {
      if(item.kkal) {
        sumKkal += item.kkal;
      }
    }
    return sumKkal;
  }
}

const hamburger = new Hamburger(SIZE_LARGE, STUFFING_CHEESE)

hamburger.addTopping(TOPPING_MAYO);

console.log("Calories: %f", hamburger.calculateCalories());

console.log("Price: %f", hamburger.calculatePrice());

hamburger.addTopping(TOPPING_SPICE);

console.log("Price with sauce: %f", hamburger.calculatePrice());

console.log("Is hamburger large: %s", hamburger.getSize() === hamburger.SIZE_LARGE);

hamburger.removeTopping(TOPPING_SPICE);

console.log("Have %d toppings", hamburger.getTopping().length);