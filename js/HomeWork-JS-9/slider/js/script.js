const slides = document.querySelectorAll('.slider__img'),
      prev = document.querySelector('.previous'),
      next = document.querySelector('.next'),
      slidesWrapper = document.querySelector('.slides__outer'),
      slidesField = document.querySelector('.slider__imgs'),
      width = window.getComputedStyle(slidesWrapper).width;
let slideIndex = 1;

let offset = 0;

slidesField.style.width = 100 * slides.length + '%';
slidesField.style.display = 'flex';
slidesField.style.transition = '1s all';
slides.forEach(slide => {
  slide.style.width = width;
})

slidesWrapper.style.overflow = 'hidden';

next.addEventListener('click', () => {
  if(offset == +width.slice(0, width.length - 2) * (slides.length - 1)) {
    offset = 0;
  } else {
    offset += +width.slice(0, width.length - 2);
  }
  slidesField.style.transform = `translateX(-${offset}px)`
})

prev.addEventListener('click', () => {
  if(offset == 0) {
    offset = +width.slice(0, width.length - 2) * (slides.length - 1);
  } else {
    offset -= +width.slice(0, width.length - 2);
  }
  slidesField.style.transform = `translateX(-${offset}px)`
})

// showSlides(slideIndex);

// function showSlides(n) {
//   if(n > slides.length) {
//     slideIndex = 1;
//   }
//   if(n < 1) {
//     slideIndex = slides.length;
//   }

//   slides.forEach(item => {
//     item.style.display = 'none';
//   })
//   slides[slideIndex - 1].style.display = '';
// }

// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }

// prev.addEventListener("click", () => {
//   plusSlides(-1);
// })

// next.addEventListener("click", () => {
//   plusSlides(1);
// })