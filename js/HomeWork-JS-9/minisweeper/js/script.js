startGame(8,8,5)

function startGame(WIDTH, HEIGHT, BOMBS_COUNT) {
  const field = document.querySelector('.field');
  const button = document.querySelector('.restart');
  const indicator = document.querySelector('.indicator');
  let flagsCount = 0;
  let newGame = false;
  const cellsCount = WIDTH * HEIGHT;
  field.innerHTML = '<button></button>'.repeat(cellsCount);
  const cells = [...field.children];

  let closedCount = cellsCount;
  indicator.innerHTML = BOMBS_COUNT + '/' + flagsCount;

  const bombs = [...Array(cellsCount).keys()]
   .sort(() => Math.random() - 0.5)
   .slice(0, BOMBS_COUNT);

   field.addEventListener('contextmenu', event => {
    if(event.target.disabled === true) {
      return;
    }
    flagsCount++;
    event.preventDefault();
    const index = cells.indexOf(event.target);
    cells[index].innerHTML = 'F';
    indicator.innerHTML = BOMBS_COUNT + '/' + flagsCount;
    return;
  })

  field.addEventListener('click', (event) => {
    button.classList.add('d-block');
    if(event.target.tagName !=='BUTTON') {
      return;
    }

    const index = cells.indexOf(event.target);
    const column = index % WIDTH;
    const row = Math.floor(index / WIDTH);
    open(row, column)
  });

  button.addEventListener('click', ()=> {
    startGame(8,8,5)
  })

  function isValid(row, column) {
    return row >=0 && row < HEIGHT && column >=0 && column < WIDTH;
  }

  function getCount(row, column) {
    let count = 0;
    for(let x = -1; x <= 1; x++) {
      for(let y = -1; y <= 1; y++) {
        if(isBomb(row + y, column + x)) {
          count++;
        }
      }
    }
    return count;
  }

  function open(row, column) {
    if(!isValid(row, column)) return;

    const index = row * WIDTH + column;
    const cell = cells[index];

    if(cell.disabled === true) return;

    cell.disabled = true;

    

    if(isBomb(row, column)) {
      cell.innerHTML = 'X';
      alert('You lose');
      showAllBomds();
      return;
    }
    closedCount--;
    if(closedCount <= BOMBS_COUNT) {
      alert('You won');
      newGame = true;
      return;
    }

    if(newGame) {
      startGame(8,8,5);
      console.log('new game')
    }

    const count = getCount(row, column);

    if(count !== 0) {
      cell.innerHTML = count;
      return;
    }
    
    for(let x = -1; x <= 1; x++) {
      for(let y = -1; y <= 1; y++) {
          open(row + y, column + x);
      }
    }
    
  }

  function showAllBomds() {
    bombs.forEach(bomb => {
      cells[bomb].innerHTML = 'X';
      cells[bomb].disabled = true;
    })
  }

  function isBomb(row, column) {
    if(!isValid(row, column)) return false;
    const index = row * WIDTH + column;

    return bombs.includes(index);
  }
}